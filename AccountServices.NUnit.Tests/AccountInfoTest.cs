﻿using AccountServices.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;


namespace AccountServices.NUnit.Tests
{
    [TestFixture]
    public class AccountInfoTest
    {
        /// <summary>
        /// Test if amount is returned from the service for a specified accountId
        /// </summary>
        /// <param name="accountId"></param>
        [Test]
        public void RefreshAmount_WhenCalled_AmountContainsValueReturnedFromService_ForAccountId([Values(200)]int accountId)
        {
            const double expectedResult = 1000.05;    
            var serviceMock = new Mock<IAccountService>();
            serviceMock.Setup(s => s.GetAccountAmount(It.Is<int>(id => id == accountId)))
                .Returns(() => expectedResult);
            var accountInfo = new AccountInfo(accountId, serviceMock.Object);

          
            accountInfo.RefreshAmount();

         
            double receivedResult = accountInfo.Amount;
            Assert.AreEqual(expectedResult, receivedResult);
        }


        /// <summary>
        /// Throw exception if AccountId is invalid
        /// </summary>
        /// <param name="invalidAccountId"></param>
        [Test]
        public void RefreshAmount_AccountIdInvalid_ThrowsException([Values(-1)]int invalidAccountId)
        {
            var serviceMock = new Mock<IAccountService>();

            serviceMock.Setup(s => s.GetAccountAmount(It.Is<int>(i => i == invalidAccountId)))
                .Throws<InvalidOperationException>();
            var accountInfo = new AccountInfo(invalidAccountId, serviceMock.Object);


            Assert.That(() => accountInfo.RefreshAmount(),
                        Throws.TypeOf<InvalidOperationException>());
        }

    }
}

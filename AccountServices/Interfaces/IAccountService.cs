﻿namespace AccountServices.Interfaces
{
    public interface IAccountService
    {
        double GetAccountAmount(int accountId);
    }
}
